package com.tsystems.javaschool.tasks.calculator;

import java.io.*;
import java.util.*;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class Calculator {
    //
    // формат записи результата 
    public static final String OUT_NUMBER_PATTERN = "#.####";
    //-----------------------------------------------------------------------	
    /**
    * Преобразовать строку в обратную польскую нотацию
    * @param sIn Входная строка
    * @return Выходная строка в обратной польской нотации
    */
    private static String opn(String sIn) {
        StringBuilder sbStack = new StringBuilder(""), sbOut = new StringBuilder("");
        char cIn, cTmp;
        // обрабатываем каждый символ последовательно
        for (int i = 0; i < sIn.length(); i++) {
            // берем текущий символ
            cIn = sIn.charAt(i);
            // если текущий символ - знак операции
            if (isOp(cIn)) {
                while (sbStack.length() > 0) {
                    // читаем символ, находящийся на вершине стека
                    cTmp = sbStack.substring(sbStack.length()-1).charAt(0);
                    // Если символ, находящийся на вершине стека имеет приоритет, больший или равный
                    // приоритету текущего символа
                    if (isOp(cTmp) && (opPrior(cIn) <= opPrior(cTmp))) {
                        // извлекаем символ из стека в выходную строку
                        sbOut.append(" ").append(cTmp).append(" ");
                        sbStack.setLength(sbStack.length()-1);
                    } else {
                        break;
                    }
                }
                // когда знак операции, в выходную строку добавляем пробел в конец
                sbOut.append(" ");
                // знак операции всегда добавляем в стек
                sbStack.append(cIn);
            // если текущий символ - открывающая скобка, то помещаем ее в стек
            } else if ('(' == cIn) {
                sbStack.append(cIn);
            // если текущий символ - закрывающая скобка
            } else if (')' == cIn) {
                cTmp = sbStack.substring(sbStack.length()-1).charAt(0);
                while ('(' != cTmp) {
                    //  извлекаем из стека в выходную строку символ
                    sbOut.append(" ").append(cTmp);
                    sbStack.setLength(sbStack.length()-1);
                    cTmp = sbStack.substring(sbStack.length()-1).charAt(0);
                }
                // убираем пробел у "("
                sbStack.setLength(sbStack.length()-1);
            } else {
                // Если символ не оператор и не скобка (т.е. число или переменная), 
                // то добавляем в выходную последовательность
                sbOut.append(cIn);
            }
        }

        // Если в стеке остались операторы, добавляем их в входную строку
        while (sbStack.length() > 0) {
            sbOut.append(" ").append(sbStack.substring(sbStack.length()-1));
            sbStack.setLength(sbStack.length()-1);
        }

        return  sbOut.toString();
    }
    //-----------------------------------------------------------------------
    /**
    * Проверка правильности расстановки скобок
    * @param s Входная строка
    * @return boolean: true - скобки расставлены верно, false - скобки расставлены неверно
    */	
    public static boolean isBracketsValid(String s) {
        char c;
        int a = 0;
        for (int i = 0; i < s.length(); i++) {
            c = s.charAt(i);
            if (c == '(') a += 1;
            if (c == ')') a -= 1;
            if (a < 0) return false;
        }
        if (a == 0) return true;
        else return false;
    }
    //-----------------------------------------------------------------------
    /**
    * Функция проверяет, является ли данный символ допустимым оператором
    * @param c Входной символ
    * @return boolean
    */		 
    private static boolean isOp(char c) {
        switch (c) {
            case '-':
            case '+':
            case '*':
            case '/':
                return true;
        }
        return false;
    }
    //-----------------------------------------------------------------------
    /**
     * Возвращает приоритет операции
     * @param op char Входной символ оператор
     * @return byte
     */
    private static byte opPrior(char op) {
        switch (op) {
            case '*':
            case '/':
                return 2;
        }
        return 1; // Тут остается + и -
    }

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
       if (statement == null || statement == "") {
           return null;
       }
            String s = statement;
            double dA = 0, dB = 0, dRes = 0;
            String sTmp;
            Deque<Double> stack = new ArrayDeque<Double>();
            // обработка начала строки на наличие унарной операции "-"
            if (s.charAt(0)=='-') {
                s="0"+s;
            }
            // удаление лишних пробелов
            s = s.replace(" ", "");
            // удаление унарной операции "-" после открывающейся скобки
            s = s.replace("(-", "(0-");	
            // проверка правильности расстановки скобок
            if (!isBracketsValid(s)) return null;
            // преобразуем строку в обратную польскую нотацию
            s = opn(s);	
            StringTokenizer st = new StringTokenizer(s);
            while(st.hasMoreTokens()) {
                try {
                    sTmp = st.nextToken().trim();  // trim() - отсечь на концах строки пустые символы;
                    // если текущий символ является оператором
                    if (sTmp.length() == 1 && isOp(sTmp.charAt(0))) {

                        if (stack.size() < 2) {   
                            // неверное количество данных в стеке для операции
                            return null;
                        }
                        dB = stack.pop();
                        dA = stack.pop();
                        switch (sTmp.charAt(0)) {
                            case '+':
                                dA += dB;
                                break;
                            case '-':
                                dA -= dB;
                                break;
                            case '/':
                                if (dB == 0) {
                                    return null;
                                }
                                dA /= dB;
                                break;
                            case '*':
                                dA *= dB;
                                break;		
                        }
                        stack.push(dA);
                    } else {
                        dA = Double.parseDouble(sTmp);
                        stack.push(dA);
                    }
                } catch (Exception e) {
                    // недопустимый символ в выражении
                    return null;
                }
            }

            if (stack.size() > 1) {
                // количество операторов не соответствует количеству операндов
                return null;
            }
            dRes = stack.pop();
            // округление производится до 4-го знака после запятой, округляется только конечный результат
            DecimalFormatSymbols dfs = new DecimalFormatSymbols(Locale.US);
            DecimalFormat myFormatter = new DecimalFormat(OUT_NUMBER_PATTERN, dfs);
            String sRes = myFormatter.format(dRes);
            return sRes;
        }
        //-----------------------------------------------------------------------	

}
