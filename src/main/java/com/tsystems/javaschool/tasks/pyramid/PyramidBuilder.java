package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.List;
import java.util.Collections;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbersNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given inputNumbers
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        if (inputNumbers.size() >= Integer.MAX_VALUE - 1) {
            throw new CannotBuildPyramidException();
        }
        // sort
        try {
            Collections.sort(inputNumbers);
        } catch (NullPointerException e) {
            throw new CannotBuildPyramidException();
        }
        int result = getRowsCount(9);

        int colsCount = 0;
        int rowsCount = getRowsCount(inputNumbers.size());
        if (rowsCount == 0) {
            throw new CannotBuildPyramidException();
        }

        colsCount = rowsCount * 2 - 1;

        int[][] A = new int[rowsCount][colsCount];

        int N = rowsCount - 1;
        A[0][N] = inputNumbers.get(0);

        int k = 1;
        for (int i = 1; i <= N; i++) {
            for (int j = 0; j <= i; j++) {
                A[i][N - i + 2*j] = inputNumbers.get(k);
                k++;
            }
        }

        return A;
    }


    static int getRowsCount(int inputNumbersCount){

        if (inputNumbersCount < 3) {
            return 0;
        }

        int curRow = 2;
        int curCount = 1;
        while (curCount < inputNumbersCount) {
            curCount += curRow;
            curRow++;
        }

        if (curCount == inputNumbersCount) {
            return curRow - 1;
        } else {
            return 0;
        }

    }
}
